const { cond, and, or, select, where, order_by, merge } = require('db-params-builder')
const { ext_cond, ext_select, ext_stmt, ext_orderBy } = require('./index')

function test_cond() {
  console.log('condition undefined: ', ext_cond(undefined))
  const condObj = cond('first_name', '=', 'Bob')
  console.log('condition normal: ', ext_cond(condObj))
}

function test_select() {
  const selectObjAll = select()
  console.log('select all: ', ext_select(selectObjAll.select))

  const selectObj = select('name', 'surname', 'age')
  console.log('select fields: ', ext_select(selectObj.select))
}

function test_stmt() {
  const whereObj = where(and(cond('first_name', '=', 'Bob'),
                              and(
                                or(cond('date', '>=', '2018-01-01'), cond('date', '<=', '2019-01-01')), 
                                and(cond('date', '>=', '2020-01-01'), cond('date', '<=', '2021-01-01'))
                              )
  ))
  console.log('where statement: ', ext_stmt(whereObj.where))
}

function test_orderBy() {
  const orderByObj = order_by({name: 'desc'}, {age: 'asc'}, {firstname: 'desc'})
  console.log('order by: ', ext_orderBy(orderByObj.order_by))
}

test_cond()
console.log('----------------')
test_select()
console.log('----------------')
test_stmt()
console.log('----------------')
test_orderBy()