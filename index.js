module.exports = {
  ext_cond, ext_select, ext_stmt, ext_orderBy
}

function ext_cond(cond) {
  if (cond == undefined) return ''
  const key = Object.keys(cond)[0]
  const relation = cond[key].relation
  const value = cond[key].value
  const valueInStr = (typeof value === 'string') ? `'${value}'` : value
  return `${key} ${relation} ${valueInStr}`
}

function ext_select(fields) {
  return fields.length == 1 ? fields[0] : fields.join(', ')
}

function ext_stmt(stmt) {
  if (stmt == undefined) return ''
  const result = _deepExtractor(stmt)
  return result.substring(1, result.length - 1)
}

function _deepExtractor(cond) {
  const key = Object.keys(cond)[0]
  if (key === 'or' || key === 'and') {
    let result = ''
    cond[key].forEach(ob => {
      result += _deepExtractor(ob) + ' ' + key + ' '
    })
    return '(' + result.substring(0, result.length - key.length - 2) + ')'
  }
  return ext_cond(cond)
}

function ext_orderBy(orders) {
  return (orders == undefined) ? '' : orders.map(order => {
                                                const column = Object.keys(order)[0]
                                                return `${column} ${order[column]}`
                                              }).join(', ')
}

