export function ext_cond(cond: Object): String
export function ext_select(cond: Array): String
export function ext_stmt(cond: Object): String
export function ext_orderBy(cond: Array): String